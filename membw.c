/*
 * Copyright (C) 2019 Anton Khirnov <anton@khirnov.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/time.h>

#define REPEATS_DEFAULT 10

void membw_mem_write_avx(uint8_t *dst, size_t dstlen);
void membw_mem_read_avx(uint8_t *src, size_t srclen);

static inline int64_t gettime(void)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (int64_t)tv.tv_sec * 1000000 + tv.tv_usec;
}

void print_usage(int argc, char **argv)
{
    fprintf(stderr,
            "Usage: %s <N> [repeats] [rw]\n"
            "\tN: array size in kB\n",
            argv[0]);
}

int main(int argc, char **argv)
{
    int ret;
    size_t N;
    unsigned int repeats = REPEATS_DEFAULT;

    uint8_t *mem;

    int64_t start;
    int64_t duration = 0;

    int do_read = 1, do_write = 1;

    if (argc < 2) {
        print_usage(argc, argv);
        return 1;
    }

    N = strtol(argv[1], NULL, 0);
    if (N <= 0) {
        fprintf(stderr, "Invalid N: %zu\n", N);
        print_usage(argc, argv);
        return 1;
    }
    N *= 1024;

    if (argc >= 3) {
        repeats = strtol(argv[2], NULL, 0);
        if (repeats <= 0) {
            fprintf(stderr, "Invalid repeats: %u\n", repeats);
            return 1;
        }
    }

    if (argc >= 4) {
        do_read = 0;
        do_write = 0;
        for (int i = 0; argv[3][i]; i++) {
            if (argv[3][i] == 'r')
                do_read = 1;
            if (argv[3][i] == 'w')
                do_write = 1;
        }
    }

    ret = posix_memalign((void**)&mem, 64, N);
    if (ret != 0) {
        fprintf(stderr, "Error allocating memory\n");
        return 2;
    }

    if (do_write) {
        start = gettime();

        for (unsigned int i = 0; i < repeats; i++)
            membw_mem_write_avx(mem, N);

        duration = gettime() - start;

        fprintf(stdout, "Written %u x %g MB in %g ms: write bandwidth: %g MB/s\n",
                repeats, (double)N / (1024 * 1024), duration / 1e3,
                (double)N * repeats / duration * 1e6 / (1024 * 1024));
    }

    if (do_read) {
        start = gettime();

        for (unsigned int i = 0; i < repeats; i++)
            membw_mem_read_avx(mem, N);

        duration = gettime() - start;

        fprintf(stdout, "Read %u x %g MB in %g ms: read bandwidth: %g MB/s\n",
                repeats, (double)N / (1024 * 1024), duration / 1e3,
                (double)N * repeats / duration * 1e6 / (1024 * 1024));
    }

    free(mem);

    return 0;
}
