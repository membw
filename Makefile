TARGET = membw

CFLAGS = -std=c99 -D_XOPEN_SOURCE=700 -fPIC -g -I. -O3
TARGET_LDFLAGS = -lm
CC = cc

OBJS =                  \
    membw.o				\
    readwrite.o         \


all: $(TARGET)

$(TARGET): $(OBJS)
	$(CC) ${TARGET_LDFLAGS} -o $@ $(OBJS)

%.o: %.c
	$(CC) $(CFLAGS) -MMD -MF $(@:.o=.d) -MT $@ -c -o $@ $<

%.o: %.asm
	nasm -f elf64 -M $< > $(@:.o=.d)
	nasm -f elf64 -o $@ $<

clean:
	-rm -f *.o *.d $(TARGET)

-include $(OBJS:.o=.d)

.PHONY: clean
